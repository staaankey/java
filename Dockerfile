FROM openjdk:17-alpine
COPY / /spd
WORKDIR /spd
RUN chmod +x gradlew
RUN ./gradlew :bootJar
WORKDIR /spd/build/libs
EXPOSE 8080
CMD ["java","-jar", "haircutStudio-0.0.1-SNAPSHOT.jar"]
