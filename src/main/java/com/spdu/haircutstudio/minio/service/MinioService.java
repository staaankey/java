package com.spdu.haircutstudio.minio.service;

import com.spdu.haircutstudio.minio.config.MinioConfiguration;
import com.spdu.haircutstudio.minio.exception.ImageNotFoundException;
import com.spdu.haircutstudio.minio.exception.ResourceWriteException;
import io.minio.*;
import io.minio.http.Method;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class MinioService {

    private final MinioClient minioClient;
    private final MinioConfiguration minioConfiguration;

    public MinioService(MinioClient minioClient, MinioConfiguration minioConfiguration) {
        this.minioClient = minioClient;
        this.minioConfiguration = minioConfiguration;
    }

    public void createBucket(String bucketName) {
        try {
            MakeBucketArgs makeBucketArgs = MakeBucketArgs.builder()
                    .bucket(bucketName)
                    .build();
            minioClient.makeBucket(makeBucketArgs);
        } catch (Exception e) {
            throw new ResourceWriteException("Cannot create bucket with name: " + bucketName);
        }
    }

    public void uploadImage(MultipartFile multipartFile, String bucketName) {
        try (InputStream inputStream = new ByteArrayInputStream(multipartFile.getBytes())) {
            minioClient.putObject(
                    PutObjectArgs.builder()
                            .bucket(bucketName)
                            .object(multipartFile.getOriginalFilename())
                            .stream(inputStream, -1, minioConfiguration.getImageSize())
                            .contentType(multipartFile.getContentType())
                            .build()
            );
        } catch (Exception e) {
            throw new ResourceWriteException(e.getMessage());
        }
    }

    public InputStream downloadImage(String bucketName, String objectName) {
        try {
            GetObjectArgs getObjectArgs = GetObjectArgs.builder()
                    .bucket(bucketName)
                    .object(objectName)
                    .build();
            return minioClient.getObject(getObjectArgs);
        } catch (Exception e) {
            throw new ImageNotFoundException(String.format("Cannot find image with name: %s", objectName));
        }
    }

    public void removeImage(String bucketName, String imageName) {
        try {
            RemoveObjectArgs removeObjectArgs = RemoveObjectArgs.builder()
                    .bucket(bucketName)
                    .object(imageName)
                    .build();
            minioClient.removeObject(removeObjectArgs);
        } catch (Exception e) {
            throw new ResourceWriteException(String.format("Cannot remove image: %s, in bucket: %s", imageName, bucketName));
        }
    }

    public String getObjectUrl(String bucketName, String imageName) {
        Map<String, String> reqParams = new HashMap<>();
        reqParams.put("response-content-type", "application/json");

        try {
            return minioClient.getPresignedObjectUrl(
                    GetPresignedObjectUrlArgs.builder()
                            .method(Method.GET)
                            .bucket(bucketName)
                            .object(imageName)
                            .expiry(24, TimeUnit.HOURS)
                            .extraQueryParams(reqParams)
                            .build());
        } catch (Exception e) {
            throw new ImageNotFoundException(String.format("Can't find image: %s in bucket: %s, please provide correct data!", imageName, bucketName));
        }
    }
}

