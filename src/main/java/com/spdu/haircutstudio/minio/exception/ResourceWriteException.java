package com.spdu.haircutstudio.minio.exception;

public class ResourceWriteException extends RuntimeException {
    public ResourceWriteException(String message) {
        super(message);
    }
}
