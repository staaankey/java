package com.spdu.haircutstudio.minio.controller;

import com.spdu.haircutstudio.minio.service.MinioService;
import org.apache.commons.compress.utils.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RequestMapping("/minio/buckets")
@RestController
public class MinioController {

    private final MinioService minioService;

    public MinioController(MinioService minioService) {
        this.minioService = minioService;
    }

    @GetMapping(value = "/{bucketName}/{imageName}", produces = {
            MediaType.IMAGE_PNG_VALUE,
            MediaType.IMAGE_JPEG_VALUE
    }
    )

    public byte[] downloadFile(@PathVariable("bucketName") String bucketName, @PathVariable("imageName") String imageName) throws IOException {
        return IOUtils.toByteArray(minioService.downloadImage(bucketName, imageName));
    }

    @PostMapping("/upload/{bucketName}")
    @ResponseStatus(HttpStatus.CREATED)
    public String uploadFile(@RequestParam MultipartFile file, @PathVariable String bucketName) {
        minioService.uploadImage(file, bucketName);
        return String.format("Image with name %s has been uploaded", file.getOriginalFilename());
    }

    @PostMapping("/{bucketName}")
    @ResponseStatus(HttpStatus.CREATED)
    public String createBucket(@PathVariable String bucketName) {
        minioService.createBucket(bucketName);
        return String.format("Bucket with name %s has been created", bucketName);
    }

    @DeleteMapping("/{bucketName}/{imageName}")
    public String deleteFile(@PathVariable("bucketName") String bucketName, @PathVariable("imageName") String imageName) {
        minioService.removeImage(bucketName, imageName);
        return String.format("Image: %s in bucket: %s has been deleted", imageName, bucketName);
    }

    @GetMapping("/link/{bucketName}/{imageName}")
    public String getImageLink(@PathVariable("bucketName") String bucketName, @PathVariable("imageName") String imageName) {
        return minioService.getObjectUrl(bucketName, imageName);
    }

}

