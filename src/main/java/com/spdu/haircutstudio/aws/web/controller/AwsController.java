package com.spdu.haircutstudio.aws.web.controller;

import com.spdu.haircutstudio.aws.service.AwsService;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/aws/buckets")
public class AwsController {

    private final AwsService service;

    public AwsController(AwsService service) {
        this.service = service;
    }

    @PostMapping("/upload")
    public String uploadFile(@RequestParam(value = "file") MultipartFile file) {
        return service.uploadImage(file) ? "File uploaded successfully" : "File hasn't been uploaded";
    }

    @GetMapping(value = "/download/{fileName}", produces = {
            MediaType.IMAGE_PNG_VALUE,
            MediaType.IMAGE_JPEG_VALUE
    }
    )
    public byte[] downloadFile(@PathVariable String fileName) {
        return service.downloadImage(fileName);
    }

    @DeleteMapping("/delete/{fileName}")
    public String deleteFile(@PathVariable String fileName) {
        service.deleteImage(fileName);
        return fileName + "Has been deleted";
    }

    @PostMapping("/bucket/{bucketName}")
    public String createBucket(@PathVariable String bucketName) {
        service.createBucket(bucketName);
        return String.format("Bucket with name %s has been created", bucketName);
    }
}
