package com.spdu.haircutstudio.aws.exception;

public class ConvertingToByteArrayException extends RuntimeException {
    public ConvertingToByteArrayException(String message) {
        super(message);
    }
}
