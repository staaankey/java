package com.spdu.haircutstudio.aws.exception;

public class ConvertingMultipartFileException extends RuntimeException {
    public ConvertingMultipartFileException(String message) {
        super(message);
    }
}
