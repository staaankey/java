package com.spdu.haircutstudio.aws.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.util.IOUtils;
import com.spdu.haircutstudio.aws.exception.ConvertingMultipartFileException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@Service
public class AwsService {

    @Value("${application.bucket.name}")
    private String bucketName;

    private final AmazonS3 s3Client;

    public AwsService(AmazonS3 s3Client) {
        this.s3Client = s3Client;
    }

    public boolean uploadImage(MultipartFile file) {
        File fileObj = convertMultiPartFileToFile(file);
        s3Client.putObject(new PutObjectRequest(bucketName, file.getOriginalFilename(), fileObj));
        return fileObj.delete();
    }


    public byte[] downloadImage(String fileName) {
        S3Object s3Object = s3Client.getObject(bucketName, fileName);
        S3ObjectInputStream inputStream = s3Object.getObjectContent();
        try {
            return IOUtils.toByteArray(inputStream);
        } catch (IOException e) {
            throw new ConvertingMultipartFileException("Unable to handle byte input stream");
        }
    }


    public void deleteImage(String fileName) {
        s3Client.deleteObject(bucketName, fileName);
    }

    public void createBucket(String bucketName) {
        s3Client.createBucket(bucketName);
    }

    private File convertMultiPartFileToFile(MultipartFile file) {
        final String fileName = file.getOriginalFilename();
        final File convertedFile = new File(fileName != null ? fileName : "test.txt");
        try (FileOutputStream fos = new FileOutputStream(convertedFile)) {
            fos.write(file.getBytes());
        } catch (IOException e) {
            throw new ConvertingMultipartFileException("Error converting multipartFile to file");
        }
        return convertedFile;
    }
}

