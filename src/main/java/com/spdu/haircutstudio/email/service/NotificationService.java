package com.spdu.haircutstudio.email.service;

import com.spdu.haircutstudio.registration.entity.User;
import com.spdu.haircutstudio.reservation.entity.Timeslot;
import com.spdu.haircutstudio.security.TokenManager;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeFormatter;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class NotificationService {
    private final JavaMailSender mailSender;
    private final TokenManager tokenManager;
    private final UserDetailsService userDetailsService;
    private final DateTimeFormatter ONLY_TIME = DateTimeFormatter.ofPattern("HH:mm:ss");
    private final DateTimeFormatter ONLY_DATE = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public NotificationService(JavaMailSender mailSender, TokenManager tokenManager, UserDetailsService userDetailsService) {
        this.mailSender = mailSender;
        this.tokenManager = tokenManager;
        this.userDetailsService = userDetailsService;
    }

    public void sendRegistrationNotification(User user) {
        ExecutorService emailExecutor = Executors.newSingleThreadExecutor();
        emailExecutor.execute(() -> {
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setSubject("Registration completed");
            mailMessage.setText("Dear %s, congratulations with completed registration at our service. \nYour token is %s".formatted(user.getFullName(), tokenManager.generateJwtToken(
                    userDetailsService.loadUserByUsername(user.getUsername()))));
            mailMessage.setTo(user.getAddress());
            mailMessage.setFrom("barber.com");
            mailSender.send(mailMessage);
        });
        emailExecutor.shutdown();
    }

    public void sendAppointmentNotification(User user, Timeslot timeslot) {
        ExecutorService emailExecutor = Executors.newSingleThreadExecutor();
        emailExecutor.execute(() -> {
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setSubject("You successfully made your appointment");
            mailMessage.setText("Dear %s, you signed up for a haircut from %s to %s %s".formatted(user.getFullName(), timeslot.getStartTime().format(ONLY_TIME), timeslot.getEndTime().format(ONLY_TIME), timeslot.getDay().format(ONLY_DATE)));
            mailMessage.setTo(user.getAddress());
            mailMessage.setFrom("barber.com");
            mailSender.send(mailMessage);
        });
        emailExecutor.shutdown();
    }
}
