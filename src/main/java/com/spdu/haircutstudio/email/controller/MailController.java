package com.spdu.haircutstudio.email.controller;

import com.spdu.haircutstudio.email.service.NotificationService;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/notifications")
public class MailController {
    private final NotificationService notificationService;

    public MailController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }
}
