package com.spdu.haircutstudio.security;

import java.util.*;

import com.amazonaws.services.dynamodbv2.xspec.S;
import com.spdu.haircutstudio.registration.repository.PersistenceUserRepository;
import com.spdu.haircutstudio.registration.service.UserService;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
@Service
public class JwtUserDetailsService implements UserDetailsService {
    private final PersistenceUserRepository userRepository;


    public JwtUserDetailsService(PersistenceUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        var user = userRepository.getUser(username).orElseThrow(() -> new UsernameNotFoundException(String.format("Username with name: %s doesn't exist", username)));
        return new User(user.getUsername(), user.getPassword(), getAuthority(userRepository.getRoleById(user.getRole())));
    }

    private Set getAuthority(String role) {
        Set<Object> roles = new HashSet<>();
        roles.add(new SimpleGrantedAuthority(role));
        return roles;
    }
    
}
