package com.spdu.haircutstudio.registration.entity;

import javax.persistence.*;

@Entity
@Table(name = "staff")
public class Staff {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "staff_id")
    private int staff_id;
    @Column(name = "profile_picture")
    private String profilePicture;
    @Column(name = "full_name")
    private String fullName;
    private String phone;
    private Integer role;
    private String city;
    private String address;
    @Column(name = "studio_id")
    private int studioId;
    @Column(name = "user_id")
    private int userId;

    public Staff() {

    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public String getFullName() {
        return fullName;
    }

    public String getPhone() {
        return phone;
    }

    public Integer getRole() {
        return role;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public int getStudioId() {
        return studioId;
    }

    public int getUserId() {
        return userId;
    }

    public void setStaff_id(int staff_id) {
        this.staff_id = staff_id;
    }

    public int getStaff_id() {
        return staff_id;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setStudioId(int studioId) {
        this.studioId = studioId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
