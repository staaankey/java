package com.spdu.haircutstudio.registration.repository;

import com.amazonaws.services.appflow.model.DatadogSourceProperties;
import com.amazonaws.services.dynamodbv2.xspec.S;
import com.spdu.haircutstudio.registration.entity.Staff;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

@Repository
public class StaffRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;

    public StaffRepository(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public Staff saveNewStaff(Staff staff) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        final String SQL_INSERT = "insert into staff(full_name, phone, role_id, " +
                                                    "city, address, user_id, " +
                                                    "profile_picture, studio_id) values " +
                "(:fullName, :phone, :role, " +
                ":city, :address, :userId," +
                ":profilePicture, :studioId)";
        jdbcTemplate.update(SQL_INSERT, new MapSqlParameterSource()
                .addValue("fullName", staff.getFullName())
                .addValue("phone", staff.getPhone())
                .addValue("role", staff.getRole())
                .addValue("city", staff.getCity())
                .addValue("address", staff.getAddress())
                .addValue("userId", staff.getUserId())
                .addValue("profilePicture", staff.getProfilePicture())
                .addValue("studioId", staff.getStudioId()), keyHolder, new String[] {"staff_id"});
        staff.setStaff_id(keyHolder.getKey().intValue());
        return staff;

    }
}
