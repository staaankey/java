package com.spdu.haircutstudio.registration.repository;


import com.spdu.haircutstudio.registration.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {

    Integer save(User user);

    List<User> findAll();

    User findOne(int id);

    void delete(Long id);

    Optional<User> getUser(String username);

    String getRoleById(Integer roleId);
}
