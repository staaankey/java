package com.spdu.haircutstudio.registration.repository;

import com.spdu.haircutstudio.registration.entity.Studio;

import java.util.List;

public interface StudioRegistrationRepos {

    int save(Studio studio);

    List<Studio> findAll();

    Studio findOne(Integer id);

    void delete(Long id);
}
