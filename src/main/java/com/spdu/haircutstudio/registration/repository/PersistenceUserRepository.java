package com.spdu.haircutstudio.registration.repository;

import com.spdu.haircutstudio.registration.entity.User;
import com.spdu.haircutstudio.registration.util.RoleRowMapper;
import com.spdu.haircutstudio.registration.util.UserRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import javax.sql.DataSource;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class PersistenceUserRepository implements UserRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final UserRowMapper rowMapper;

    @Autowired
    public PersistenceUserRepository(
            DataSource dataSource,
            UserRowMapper rowMapper) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.rowMapper = rowMapper;
    }

    @Override
    public Integer save(User user) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        final String SQL_INSERT = "insert into users (full_name, phone, role_id, city, address, username, password, active) values (:fullName, :phone, :role_id, :city, :address, :username, :password, :active)";
        jdbcTemplate.update(SQL_INSERT, new MapSqlParameterSource()
                .addValue("fullName", user.getFullName())
                .addValue("phone", user.getPhone())
                .addValue("role_id", user.getRole())
                .addValue("city", user.getCity())
                .addValue("address", user.getAddress())
                .addValue("username", user.getUsername())
                .addValue("password", user.getPassword())
                .addValue("active", user.getActive()), keyHolder, new String[] {"user_id"});
        return keyHolder.getKey().intValue();

    }

    @Override
    public List<User> findAll() {
        return jdbcTemplate.query("select * from users", rowMapper);
    }

    @Override
    public User findOne(int userId) {
        User user = null;
        try {
            user = jdbcTemplate.queryForObject("select * from users where user_id = :userId", new MapSqlParameterSource()
                    .addValue("userId", userId), rowMapper);
        } catch (DataAccessException dataAccessException) {
            System.out.println("Couldn't find entity of type Person with id:" + userId);
        }
        return user;
    }

    @Override
    public void delete(Long userId) {
        //jdbcTemplate.update("delete from timeslot where user_id = :userId and );
    }

    @Override
    public Optional<User> getUser(String username) {
        var sql = "SELECT * FROM users WHERE username=:username";
        Map<String, String> parameters = Map.ofEntries(Map.entry("username", username));
        return jdbcTemplate.query(sql, parameters, rowMapper).stream().findFirst();

    }

    @Override
    public String getRoleById(Integer roleId) {
        var sql = "SELECT * FROM role_table WHERE id=:roleId";
        var roleId1 = jdbcTemplate.queryForObject(sql, new MapSqlParameterSource()
                .addValue("roleId", roleId), new RoleRowMapper());
        return roleId1.getName();
    }
}
