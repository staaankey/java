package com.spdu.haircutstudio.registration.repository;


import com.spdu.haircutstudio.registration.entity.Studio;
import com.spdu.haircutstudio.registration.util.StudioRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;


@Repository
public class PersistenceStudioRegistrationRepos implements StudioRegistrationRepos {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public PersistenceStudioRegistrationRepos(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public List<Studio> findAll() {
        return jdbcTemplate.query("select * from studios", new StudioRowMapper());
    }

    @Override
    public Studio findOne(Integer studioId) {
        Studio studio = null;
        try {
            studio = jdbcTemplate.queryForObject("select * from studios where studio_id = :studioId", new MapSqlParameterSource()
                    .addValue("studioId", studioId), new StudioRowMapper());
        } catch (DataAccessException dataAccessException) {
            System.err.println("\"Couldn't find entity of type Person with id {}" + studioId);
        }
        return studio;
    }

    @Override
    public int save(Studio studio) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        final String SQL_INSERT = "insert into studios(name, url, phone, start_time, end_time) values (:name, :url, :phone, :startTime, :endTime)";
        jdbcTemplate.update(SQL_INSERT, new MapSqlParameterSource()
                .addValue("name", studio.getName())
                .addValue("url", studio.getUrl())
                .addValue("phone", studio.getPhone())
                .addValue("startTime", studio.getStartTime())
                .addValue("endTime", studio.getEndTime()), keyHolder, new String[] {"studio_id"});
        return keyHolder.getKey().intValue();
    }

    @Override
    public void delete(Long studioId) {
        jdbcTemplate.update("delete from studios where studio_id = :studioId", new MapSqlParameterSource()
                .addValue("studioId", studioId));
    }
}


