package com.spdu.haircutstudio.registration.web.dto;

public final class UserDtoBuilder {
    private String fullName;
    private String phone;
    private Integer role;
    private String city;
    private String address;
    private String password;
    private String username;
    private Boolean active;

    private UserDtoBuilder() {
    }

    public static UserDtoBuilder anUserDto() {
        return new UserDtoBuilder();
    }

    public UserDtoBuilder withFullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public UserDtoBuilder withPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public UserDtoBuilder withCity(String city) {
        this.city = city;
        return this;
    }

    public UserDtoBuilder withAddress(String address) {
        this.address = address;
        return this;
    }

    public UserDtoBuilder withPassword(String password) {
        this.password = password;
        return this;
    }

    public UserDtoBuilder withUsername(String username) {
        this.username = username;
        return this;
    }

    public UserDtoBuilder withActive(Boolean active) {
        this.active = active;
        return this;
    }

    public UserDto build() {
        UserDto userDto = new UserDto();
        userDto.setFullName(fullName);
        userDto.setPhone(phone);
        userDto.setCity(city);
        userDto.setAddress(address);
        userDto.setPassword(password);
        userDto.setUsername(username);
        userDto.setActive(active);
        return userDto;
    }
}
