package com.spdu.haircutstudio.registration.web.dto;

import java.time.LocalDateTime;

public final class StudioDtoBuilder {
    private String name;
    private String phone;
    private String url;
    private LocalDateTime startTime;
    private LocalDateTime endTime;

    private StudioDtoBuilder() {
    }

    public static StudioDtoBuilder aStudioDto() {
        return new StudioDtoBuilder();
    }

    public StudioDtoBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public StudioDtoBuilder withPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public StudioDtoBuilder withUrl(String url) {
        this.url = url;
        return this;
    }

    public StudioDtoBuilder withStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
        return this;
    }

    public StudioDtoBuilder withEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
        return this;
    }

    public StudioDto build() {
        StudioDto studioDto = new StudioDto();
        studioDto.setName(name);
        studioDto.setPhone(phone);
        studioDto.setUrl(url);
        studioDto.setStartTime(startTime);
        studioDto.setEndTime(endTime);
        return studioDto;
    }
}
