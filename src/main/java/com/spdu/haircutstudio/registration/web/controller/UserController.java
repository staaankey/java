package com.spdu.haircutstudio.registration.web.controller;

import com.spdu.haircutstudio.registration.entity.User;
import com.spdu.haircutstudio.registration.service.UserServiceImpl;
import com.spdu.haircutstudio.registration.web.dto.UserDto;
import com.spdu.haircutstudio.registration.web.dto.UserResultDto;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserServiceImpl userService;

    public UserController(UserServiceImpl userService) {
        this.userService = userService;
    }

    @PostMapping("/new")
    @ResponseStatus(HttpStatus.CREATED)
    public UserResultDto registerUser(@Valid @RequestBody UserDto userDto) {
        var userResultDto = new UserResultDto();
        BeanUtils.copyProperties(userDto, userResultDto);
        userResultDto.setUserId(userService.register(convertToEntity(userDto)));
        return userResultDto;
    }

    @GetMapping("/all")
    public List<User> getAllUsers() {
        return userService.findUsers();
    }

    @GetMapping("/{id}")
    public User getUser(@PathVariable int id) {
        return userService.findUser(id);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
    }

    private User convertToEntity(UserDto userDto) {
        User user = new User();
        BeanUtils.copyProperties(userDto, user);
        user.setRole(1);
        return user;
    }

}
