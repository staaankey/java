package com.spdu.haircutstudio.registration.web.dto;

import java.io.Serializable;
import java.util.Objects;

public class UserResultDto {
    private int userId;
    private String fullName;
    private String phone;
    private Integer role;
    private String city;
    private String address;
    private String username;
    private String password;
    private Boolean active;

    public UserResultDto() {
        this.role = 1;
    }



    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getUserId() {
        return userId;
    }

    public String getFullName() {
        return fullName;
    }

    public String getPhone() {
        return phone;
    }

    public Integer getRole() {
        return role;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserResultDto entity = (UserResultDto) o;
        return Objects.equals(this.userId, entity.userId) &&
                Objects.equals(this.fullName, entity.fullName) &&
                Objects.equals(this.phone, entity.phone) &&
                Objects.equals(this.role, entity.role) &&
                Objects.equals(this.city, entity.city) &&
                Objects.equals(this.address, entity.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, fullName, phone, role, city, address);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "userId = " + userId + ", " +
                "fullName = " + fullName + ", " +
                "phone = " + phone + ", " +
                "role = " + role + ", " +
                "city = " + city + ", " +
                "address = " + address + ")";
    }
}
