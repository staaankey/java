package com.spdu.haircutstudio.registration.web.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class StudioDto {
    private String name;
    private String phone;
    private String url;
    private LocalDateTime startTime;
    private LocalDateTime endTime;

    public StudioDto() {
    }

    public LocalDateTime getEndTime() {
        endTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH"));
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public LocalDateTime getStartTime() {
        startTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH"));
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
