package com.spdu.haircutstudio.registration.web.dto;


public class StaffResultDto {
    private int staffId;
    private String profilePicture;
    private String fullName;
    private String phone;
    private Integer role;
    private String city;
    private String address;
    private int studioId;
    private int userId;

    public StaffResultDto() {
    }

    public int getStaffId() {
        return staffId;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public String getFullName() {
        return fullName;
    }

    public String getPhone() {
        return phone;
    }

    public Integer getRole() {
        return role;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public int getStudioId() {
        return studioId;
    }

    public int getUserId() {
        return userId;
    }

    public void setStaffId(int staffId) {
        this.staffId = staffId;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setStudioId(int studioId) {
        this.studioId = studioId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}

