package com.spdu.haircutstudio.registration.web.controller;

import com.spdu.haircutstudio.registration.entity.Studio;
import com.spdu.haircutstudio.registration.service.StudioRegistrationServiceImpl;
import com.spdu.haircutstudio.registration.web.dto.StudioDto;
import com.spdu.haircutstudio.registration.web.dto.StudioResultDto;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/studios")
public class StudioController {
    private final StudioRegistrationServiceImpl registrationService;

    public StudioController(StudioRegistrationServiceImpl registrationService) {
        this.registrationService = registrationService;
    }

    @GetMapping("/all")
    public List<Studio> getAllStudios() {
        return registrationService.findAllStudios();
    }

    @GetMapping("/{id}")
    public Studio getStudio(@PathVariable Integer id) {
        return registrationService.findOne(id);
    }

    @DeleteMapping("/{id}")
    public void deleteStudio(@PathVariable Long id) {
        registrationService.deleteStudio(id); //TODO: referenced from table "staff"
    }

    @PostMapping("/new")
    @ResponseStatus(HttpStatus.CREATED)
    public StudioResultDto createStudio(@RequestBody StudioDto studioDto) {
        var studioResultDto = new StudioResultDto();
        BeanUtils.copyProperties(studioDto, studioResultDto);
        studioResultDto.setStartTime(studioDto.getStartTime().toLocalTime());
        studioResultDto.setEndTime(studioDto.getEndTime().toLocalTime());
        studioResultDto.setId(registrationService.register(convertToEntity(studioDto)));
        return studioResultDto;
    }

    private Studio convertToEntity(StudioDto studioDto) {
        Studio studio = new Studio();
        BeanUtils.copyProperties(studioDto, studio);
        studio.setStartTime(studioDto.getStartTime().toLocalTime());
        studio.setEndTime(studioDto.getEndTime().toLocalTime());
        return studio;
    }

}
