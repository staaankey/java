package com.spdu.haircutstudio.registration.web.dto;

public final class StaffDtoBuilder {
    private String profilePicture;
    private String fullName;
    private String phone;
    private Integer roleId;
    private String city;
    private String address;
    private int studioId;
    private int userId;

    private StaffDtoBuilder() {
    }

    public static StaffDtoBuilder aStaffDto() {
        return new StaffDtoBuilder();
    }

    public StaffDtoBuilder profilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
        return this;
    }

    public StaffDtoBuilder fullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public StaffDtoBuilder phone(String phone) {
        this.phone = phone;
        return this;
    }

    public StaffDtoBuilder roleId(Integer roleId) {
        this.roleId = roleId;
        return this;
    }

    public StaffDtoBuilder city(String city) {
        this.city = city;
        return this;
    }

    public StaffDtoBuilder address(String address) {
        this.address = address;
        return this;
    }


    public StaffDtoBuilder studioId(int studioId) {
        this.studioId = studioId;
        return this;
    }

    public StaffDtoBuilder userId(int userId) {
        this.userId = userId;
        return this;
    }

    public StaffDto build() {
        StaffDto staffDto = new StaffDto();
        staffDto.setProfilePicture(profilePicture);
        staffDto.setFullName(fullName);
        staffDto.setPhone(phone);
        staffDto.setRoleId(roleId);
        staffDto.setCity(city);
        staffDto.setAddress(address);
        staffDto.setStudioId(studioId);
        staffDto.setUserId(userId);
        return staffDto;
    }
}
