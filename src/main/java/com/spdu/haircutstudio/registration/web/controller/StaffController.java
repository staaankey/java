package com.spdu.haircutstudio.registration.web.controller;

import com.spdu.haircutstudio.registration.entity.Staff;
import com.spdu.haircutstudio.registration.service.StaffServiceImpl;
import com.spdu.haircutstudio.registration.util.StaffConverter;
import com.spdu.haircutstudio.registration.web.dto.StaffDto;
import com.spdu.haircutstudio.registration.web.dto.StaffResultDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.spdu.haircutstudio.registration.util.StaffConverter.*;

@RestController
@RequestMapping("/staff")
public class StaffController {
    private final StaffServiceImpl staffService;

    public StaffController(StaffServiceImpl staffService) {
        this.staffService = staffService;
    }

    @PostMapping("/new")
    @ResponseStatus(HttpStatus.CREATED)
    Staff registerNewStaff(@RequestBody StaffDto dto) {
        return staffService.registerNew(toEntity(dto));
    }
}
