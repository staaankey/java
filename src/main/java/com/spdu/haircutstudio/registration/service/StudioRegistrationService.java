package com.spdu.haircutstudio.registration.service;

import com.spdu.haircutstudio.registration.entity.Studio;

import java.util.List;

public interface StudioRegistrationService {
    Integer register(Studio studio);

    List<Studio> findAllStudios();

    Studio findOne(Integer id);

    void deleteStudio(Long id);

}
