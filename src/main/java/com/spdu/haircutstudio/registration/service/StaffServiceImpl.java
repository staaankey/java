package com.spdu.haircutstudio.registration.service;

import com.spdu.haircutstudio.registration.entity.Staff;
import com.spdu.haircutstudio.registration.repository.StaffRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class StaffServiceImpl implements StaffService {
    private final StaffRepository staffRepository;

    public StaffServiceImpl(StaffRepository staffRepository) {
        this.staffRepository = staffRepository;
    }


    @Override
    public Staff registerNew(Staff staff) {
        return staffRepository.saveNewStaff(staff);
    }

}
