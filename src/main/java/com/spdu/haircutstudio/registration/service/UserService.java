package com.spdu.haircutstudio.registration.service;

import com.spdu.haircutstudio.registration.entity.User;

import java.util.List;

public interface UserService {
    Integer register(User user);

    List<User> findUsers();

    User findUser(int id);

    void deleteUser(Long id);
}
