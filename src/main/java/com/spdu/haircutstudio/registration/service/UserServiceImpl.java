package com.spdu.haircutstudio.registration.service;

import com.spdu.haircutstudio.email.service.NotificationService;
import com.spdu.haircutstudio.registration.entity.User;
import com.spdu.haircutstudio.registration.repository.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final NotificationService notificationService;
    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository, NotificationService notificationService, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.notificationService = notificationService;
        this.passwordEncoder = passwordEncoder;
    }


    @Override
    public Integer register(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        var userId = userRepository.save(user);
        notificationService.sendRegistrationNotification(user);
        return userId;

    }

    @Override
    public List<User> findUsers() {
        return userRepository.findAll();
    }

    @Override
    public User findUser(int id) {
        return userRepository.findOne(id);
    }

    @Override
    public void deleteUser(Long id) {
        userRepository.delete(id);
    }


}
