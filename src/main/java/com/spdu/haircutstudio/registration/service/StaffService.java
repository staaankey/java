package com.spdu.haircutstudio.registration.service;

import com.spdu.haircutstudio.registration.entity.Staff;

public interface StaffService {
    Staff registerNew(Staff staff);
}
