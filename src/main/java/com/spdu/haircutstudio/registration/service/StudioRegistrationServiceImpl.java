package com.spdu.haircutstudio.registration.service;

import com.spdu.haircutstudio.registration.entity.Studio;
import com.spdu.haircutstudio.registration.repository.StudioRegistrationRepos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudioRegistrationServiceImpl implements StudioRegistrationService {
    @Autowired private StudioRegistrationRepos repos;

    @Override
    public Integer register(Studio studio) {
        return repos.save(studio);
    }

    @Override
    public List<Studio> findAllStudios() {
        return repos.findAll();
    }

    @Override
    public Studio findOne(Integer id) {
        return repos.findOne(id);
    }

    @Override
    public void deleteStudio(Long  id) {
        repos.delete(id);
    }
}
