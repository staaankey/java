package com.spdu.haircutstudio.registration.util;

import com.spdu.haircutstudio.registration.entity.Staff;
import com.spdu.haircutstudio.registration.web.dto.StaffDto;
import com.spdu.haircutstudio.registration.web.dto.StaffResultDto;

public class StaffConverter {
    public static StaffResultDto toDto(Staff staff) {
        var dto = new StaffResultDto();
        dto.setAddress(staff.getAddress());
        dto.setCity(staff.getCity());
        dto.setFullName(staff.getFullName());
        dto.setPhone(staff.getPhone());
        dto.setProfilePicture(staff.getProfilePicture());
        dto.setStudioId(staff.getStudioId());
        dto.setRole(staff.getRole());
        dto.setStaffId(staff.getStaff_id());
        return dto;
    }

    public static Staff toEntity(StaffDto dto) {
        var entity = new Staff();
        entity.setAddress(dto.getAddress());
        entity.setCity(dto.getCity());
        entity.setFullName(dto.getFullName());
        entity.setPhone(dto.getPhone());
        entity.setProfilePicture(dto.getProfilePicture());
        entity.setRole(dto.getRole());
        entity.setUserId(dto.getUserId());
        entity.setStudioId(dto.getStudioId());
        return entity;

    }
}
