package com.spdu.haircutstudio.registration.util;

import com.spdu.haircutstudio.registration.entity.Role;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RoleRowMapper implements RowMapper<Role> {
    @Override
    public Role mapRow(ResultSet rs, int rowNum) throws SQLException {
        var role = new Role();
        role.setRoleId(rs.getInt("id"));
        role.setName(rs.getString("name"));
        return role;
    }
}
