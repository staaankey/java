package com.spdu.haircutstudio.registration.util;

import com.spdu.haircutstudio.registration.entity.Studio;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StudioRowMapper implements RowMapper<Studio> {
    @Override
    public Studio mapRow(ResultSet rs, int rowNum) throws SQLException {
        var studio = new Studio();
        studio.setId(rs.getInt("studio_id"));
        studio.setName(rs.getString("name"));
        studio.setPhone(rs.getString("phone"));
        studio.setUrl(rs.getString("url"));
        studio.setStartTime(rs.getTime("start_time").toLocalTime());
        studio.setEndTime(rs.getTime("end_time").toLocalTime());
        return studio;
    }
}
