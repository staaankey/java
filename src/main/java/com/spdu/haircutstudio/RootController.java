package com.spdu.haircutstudio;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class RootController {

    @GetMapping("/")
    public String hello() {
        return "login";
    }

    @GetMapping("/register/")
    public String register() {
        return "register";
    }
}
