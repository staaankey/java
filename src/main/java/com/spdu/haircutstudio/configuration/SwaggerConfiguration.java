package com.spdu.haircutstudio.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.List;

@Configuration
public class SwaggerConfiguration {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.spdu.haircutstudio"))
                .build()
                .securitySchemes(apiKeys())
                .securityContexts(List.of(securityContext()));
    }

    private List<SecurityScheme> apiKeys() {
        return List.of(new ApiKey("Authorization token", HttpHeaders.AUTHORIZATION, "header"));
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(globalAuth())
                .build();
    }

    private List<SecurityReference> globalAuth() {
        var admin = new AuthorizationScope("admin", "admin points");
        return List.of(new SecurityReference("Authorization token", new AuthorizationScope[]{admin}));
    }
}