package com.spdu.haircutstudio.reservation.repository;

import com.spdu.haircutstudio.reservation.entity.Timeslot;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Repository
public class TimeslotRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");


    public TimeslotRepository(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<Timeslot> findAllTimeslotsByUser(Integer userId) {
        return jdbcTemplate.query("select * from timeslot where user_id = :userId", new MapSqlParameterSource()
                .addValue("userId", userId), new TimeslotRowMapper());
    }


    public Timeslot saveTimeslot(Timeslot timeslot) {
        final var SQL_INSERT = "insert into timeslot (start_time, end_time, day, user_id, studio_id, staff_id) " +
                "values(:startTime, :endTime, :day, :userId, :studioId, :staffId)";
        final KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(SQL_INSERT, new MapSqlParameterSource()
                .addValue("startTime", timeslot.getStartTime())
                .addValue("endTime", timeslot.getEndTime())
                .addValue("day", timeslot.getDay())
                .addValue("userId", timeslot.getUserId())
                .addValue("studioId", timeslot.getStudioId())
                .addValue("staffId", timeslot.getStaffId()), keyHolder, new String[] {"timeslot_id"});
        timeslot.setId(keyHolder.getKey().intValue());
        return timeslot;
    }

    public List<Timeslot> getReservedSlots(LocalDate day, Integer studioId, Integer staff_id) {
        return jdbcTemplate.query("select * from timeslot where day = :day and studio_id = :studioId and staff_id = :staffId", new MapSqlParameterSource()
                .addValue("day", day)
                .addValue("studioId", studioId)
                .addValue("staffId", staff_id), new TimeslotRowMapper());
    }

    public Timeslot findTimeslot(int timeslotId) {
        return jdbcTemplate.queryForObject("select * from timeslot where timeslot_id = :timeslotId", new MapSqlParameterSource()
                .addValue("timeslotId", timeslotId), new TimeslotRowMapper());
    }

    public List<Timeslot> getAll() {
        return jdbcTemplate.query("select * from timeslot", new TimeslotRowMapper());
    }
}
