package com.spdu.haircutstudio.reservation.repository;

import com.spdu.haircutstudio.reservation.entity.Timeslot;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class TimeslotRowMapper implements RowMapper<Timeslot> {
    @Override
    public Timeslot mapRow(ResultSet rs, int rowNum) throws SQLException {
        var timeslot = new Timeslot();
        timeslot.setId(rs.getInt("timeslot_id"));
        timeslot.setDay(LocalDateTime.of(rs.getObject("day", LocalDate.class), LocalTime.parse("00:00:00")));
        timeslot.setStartTime(LocalDateTime.of(rs.getObject("day", LocalDate.class), rs.getObject("start_time", LocalTime.class)));
        timeslot.setEndTime(LocalDateTime.of(rs.getObject("day", LocalDate.class), rs.getObject("end_time", LocalTime.class)));
        timeslot.setUserId(rs.getInt("user_id"));
        timeslot.setStudioId(rs.getInt("studio_id"));
        timeslot.setStaffId(rs.getInt("staff_id"));
        return timeslot;
    }
}
