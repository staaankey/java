package com.spdu.haircutstudio.reservation.util;

import com.spdu.haircutstudio.reservation.entity.Timeslot;
import com.spdu.haircutstudio.reservation.web.dto.TimeslotDto;
import com.spdu.haircutstudio.reservation.web.dto.TimeslotResultDto;

public class TimeslotConverter {

    public static Timeslot convertToEntity(TimeslotDto dto) {
        var timeslot = new Timeslot();
        timeslot.setStartTime(dto.getStartTime());
        timeslot.setEndTime(dto.getEndTime());
        timeslot.setDay(dto.getDay());
        timeslot.setUserId(dto.getUserId());
        timeslot.setStaffId(dto.getStaffId());
        timeslot.setStudioId(dto.getStudioId());
        return timeslot;
    }

    public static TimeslotResultDto convertToDto(Timeslot timeslot) {
        var dto = new TimeslotResultDto();
        dto.setStartTime(timeslot.getStartTime());
        dto.setEndTime(timeslot.getEndTime());
        dto.setDay(timeslot.getDay());
        dto.setUserId(timeslot.getUserId());
        dto.setId(timeslot.getId());
        dto.setStaffId(timeslot.getStaffId());
        dto.setStudioId(timeslot.getStudioId());
        return dto;
    }
}
