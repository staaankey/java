package com.spdu.haircutstudio.reservation.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity(name = "timeslot")
public class Timeslot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "timeslot_id")
    private Integer id;
    @Column(name = "day", columnDefinition = "DATE")
    private LocalDateTime day;
    @Column(name = "start_time", columnDefinition = "TIME")
    private LocalDateTime startTime;
    @Column(name = "end_time", columnDefinition = "TIME")
    private LocalDateTime endTime;
    @Column(name = "user_id")
    private Integer userId;
    @Column(name = "studio_id")
    private Integer studioId;
    @Column(name = "staff_id")
    private Integer staffId;

    public Timeslot(LocalDateTime day, LocalDateTime startTime, LocalDateTime endTime, Integer studioId, Integer staffId, Integer userId) {
        this.day = day;
        this.startTime = startTime;
        this.endTime = endTime;
        this.studioId = studioId;
        this.staffId = staffId;
        this.userId = userId;
    }

    public Timeslot() {
    }


    public LocalDateTime getDay() {
        return day;
    }

    public void setDay(LocalDateTime day) {
        this.day = day;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    @JsonIgnore
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @JsonIgnore
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonIgnore
    public LocalTime getStartTimeOnly() {
        return this.startTime.toLocalTime();
    }

    public Integer getStudioId() {
        return studioId;
    }

    public void setStudioId(Integer studioId) {
        this.studioId = studioId;
    }

    public boolean checkOutdated() {
        return day.getDayOfMonth() < LocalDateTime.now().getDayOfMonth();
    }

    public Integer getStaffId() {
        return staffId;
    }

    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Timeslot timeslot = (Timeslot) o;

        if (!day.equals(timeslot.day)) return false;
        if (!startTime.equals(timeslot.startTime)) return false;
        if (!studioId.equals(timeslot.studioId)) return false;
        if (!staffId.equals(timeslot.staffId)) return false;
        return endTime.equals(timeslot.endTime);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + day.hashCode();
        result = 31 * result + startTime.hashCode();
        result = 31 * result + endTime.hashCode();
        result = 31 * result + studioId.hashCode();
        result = 31 * result * staffId.hashCode();
        return result;
    }
}