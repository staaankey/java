package com.spdu.haircutstudio.reservation.exception;

public class SaveTimeslotException extends RuntimeException {
    public SaveTimeslotException(String message) {
        super(message);
    }
}
