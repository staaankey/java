package com.spdu.haircutstudio.reservation.service;

import com.spdu.haircutstudio.email.service.NotificationService;
import com.spdu.haircutstudio.registration.repository.PersistenceStudioRegistrationRepos;
import com.spdu.haircutstudio.registration.service.UserService;
import com.spdu.haircutstudio.registration.service.UserServiceImpl;
import com.spdu.haircutstudio.reservation.entity.Timeslot;
import com.spdu.haircutstudio.reservation.exception.SaveTimeslotException;
import com.spdu.haircutstudio.reservation.repository.TimeslotRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class TimeslotService {
    private final TimeslotRepository timeslotRepository;
    private final PersistenceStudioRegistrationRepos studioRepos;
    private final NotificationService notificationService;
    private final UserServiceImpl userService;


    public TimeslotService(
            TimeslotRepository timeslotRepository,
            PersistenceStudioRegistrationRepos studioRepos,
            NotificationService notificationService,
            UserServiceImpl userService)
    {
        this.timeslotRepository = timeslotRepository;
        this.studioRepos = studioRepos;
        this.notificationService = notificationService;
        this.userService = userService;
    }

    public Timeslot registerTimeslot(Timeslot timeslot) throws SaveTimeslotException {
        if (this.getAllAvailable(timeslot.getDay(), timeslot.getStudioId(), timeslot.getStaffId(), timeslot.getUserId()).contains(timeslot)) {
            notificationService.sendAppointmentNotification(userService.findUser(timeslot.getUserId()), timeslot);
            return timeslotRepository.saveTimeslot(timeslot);
        } else {
            throw new SaveTimeslotException("Wrong data has been sent or time is already booked!");
        }
    }

    public List<Timeslot> getAllReserved(LocalDate day, Integer studioId, Integer staffId) {
        return timeslotRepository.getReservedSlots(day, studioId, staffId);
    }

    public List<Timeslot> getAllAvailable(LocalDateTime day, Integer studioId, Integer staffId, Integer userId) {
        var studio = studioRepos.findOne(studioId);
        var reservedTimeslots = timeslotRepository.getReservedSlots(day.toLocalDate(), studioId, staffId);
        var workHours = Stream.iterate(studio.getStartTime(), t -> t.plusHours(1))
                .limit(ChronoUnit.HOURS.between(studio.getStartTime(), studio.getEndTime()))
                .toList();

        Set<LocalTime> takenStartTime = reservedTimeslots.stream()
                .map(Timeslot::getStartTimeOnly)
                .collect(Collectors.toSet());


        return workHours.stream()
                .filter(dateTime -> !takenStartTime.contains(dateTime))
                .map(dateTime -> new Timeslot(day, LocalDateTime.of(day.toLocalDate(), dateTime), LocalDateTime.of(day.toLocalDate(), dateTime).plusHours(1), studioId, staffId, userId))
                .toList();
    }

    public List<Timeslot> getAllUsersTimeslots(Integer userId) {
        return timeslotRepository.findAllTimeslotsByUser(userId);
    }

    public Timeslot findTimeslot(int timeslotId) {
        return timeslotRepository.findTimeslot(timeslotId);
    }
}
