package com.spdu.haircutstudio.reservation.web.controller;

import com.spdu.haircutstudio.reservation.entity.Timeslot;
import com.spdu.haircutstudio.reservation.service.TimeslotService;
import com.spdu.haircutstudio.reservation.web.dto.TimeslotDto;
import com.spdu.haircutstudio.reservation.web.dto.TimeslotResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static com.spdu.haircutstudio.reservation.util.TimeslotConverter.convertToDto;
import static com.spdu.haircutstudio.reservation.util.TimeslotConverter.convertToEntity;

@RestController
@RequestMapping("/timeslots")
public class TimeslotController {
    private final TimeslotService timeslotService;

    @Autowired
    public TimeslotController(TimeslotService timeslotService) {
        this.timeslotService = timeslotService;
    }

    @PostMapping("/new")
    @ResponseStatus(HttpStatus.CREATED)
    Timeslot registerTimeslot(@RequestBody TimeslotDto timeslotDto) {
        return timeslotService.registerTimeslot(convertToEntity(timeslotDto));
    }

    @GetMapping("/{userId}")
    List<Timeslot> getAllTimeslots (@PathVariable Integer userId) {
        return timeslotService.getAllUsersTimeslots(userId);
    }

    @GetMapping("/allBooked")
    public List<Timeslot> getAllReserved(@RequestParam("day") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDate day,
                                         @RequestParam Integer studioId, @RequestParam Integer staffId) {
        return timeslotService.getAllReserved(day, studioId, staffId);
    }

}
