package com.spdu.haircutstudio.reservation.web.dto;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TimeslotDto {
    private LocalDateTime day;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private Integer userId;
    private Integer studioId;
    private Integer staffId;

    public TimeslotDto() {
    }

    public Integer getStudioId() {
        return studioId;
    }

    public void setStudioId(Integer studioId) {
        this.studioId = studioId;
    }

    public LocalDateTime getDay() {
        day.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        return day;
    }

    public void setDay(LocalDateTime day) {
        this.day = day;
    }

    public LocalDateTime getStartTime() {
        startTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH"));
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        endTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH"));
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getStaffId() {
        return staffId;
    }

    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }
}
