package com.spdu.haircutstudio.reservation.web.dto;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class TimeslotResultDto {
    private Integer id;
    private LocalDateTime day;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private Integer userId;
    private Integer studioId;
    private Integer staffId;

    public TimeslotResultDto() {
    }

    public Integer getStudioId() {
        return studioId;
    }

    public void setStudioId(Integer studioId) {
        this.studioId = studioId;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setDay(LocalDateTime day) {
        this.day = day;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getId() {
        return id;
    }

    public LocalDateTime getDay() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return LocalDateTime.parse(getDay().toString(), formatter);
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public Integer getUserId() {
        return userId;
    }

    public Integer getStaffId() {
        return staffId;
    }

    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimeslotResultDto entity = (TimeslotResultDto) o;
        return Objects.equals(this.id, entity.id) &&
                Objects.equals(this.day, entity.day) &&
                Objects.equals(this.startTime, entity.startTime) &&
                Objects.equals(this.endTime, entity.endTime) &&
                Objects.equals(this.userId, entity.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, day, startTime, endTime, userId);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "id = " + id + ", " +
                "day = " + day + ", " +
                "startTime = " + startTime + ", " +
                "endTime = " + endTime + ", " +
                "userId = " + userId;
    }
}
