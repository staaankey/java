CREATE TABLE "studios" (
    "studio_id" serial,
    "name" varchar(128) NOT NULL,
    "url" varchar(255) NOT NULL,
    "phone" varchar(12) NOT NULL,
    "start_time" TIME NOT NULL,
    "end_time" TIME NOT NULL,
    CONSTRAINT "studios_pk" PRIMARY KEY ("studio_id")
);

create table role_table
(
    id   serial      not null
        constraint role_table_pk
            primary key,
    name varchar(20) not null
);



CREATE TABLE "users" (
    "user_id" serial NOT NULL UNIQUE,
    "full_name" varchar(128) default(' '),
    "username" varchar(255) default(' '),
    "password" varchar(255) default (' '),
    "phone" varchar(12) default(' '),
    role_id  integer
        constraint user_table_role_table_id_fk
            references role_table,
    "city" varchar(255) default(' '),
    "address" varchar(255) default(' '),
    "active" bool,
    CONSTRAINT "users_pk" PRIMARY KEY ("user_id")
);



CREATE TABLE "staff" (
    "staff_id" serial,
    "profile_picture" varchar(255) NOT NULL,
    "studio_id" int NOT NULL,
    CONSTRAINT "staff_pk" PRIMARY KEY ("staff_id")
) INHERITS (users);


insert into role_table(name) values ('ROLE_ADMIN');
insert into role_table(name) values ('ROLE_USER');


CREATE TABLE "timeslot" (
    "timeslot_id" serial,
    "day" date NOT NULL,
    "start_time" TIME NOT NULL,
    "end_time" TIME NOT NULL,
    "user_id" serial NOT NULL,
    "studio_id" int NOT NULL,
    "staff_id" int NOT NULL,
    CONSTRAINT "timeslot_pk" PRIMARY KEY ("timeslot_id")
);



CREATE TABLE "stuff_timeslot" (
     "timeslot_id" serial,
     "staff_id" serial
);








ALTER TABLE staff add constraint "staff_fk0" foreign key ("user_id") references "users"("user_id");
ALTER TABLE "staff" ADD CONSTRAINT "staff_fk1" FOREIGN KEY ("studio_id") REFERENCES "studios"("studio_id");

ALTER TABLE "timeslot" ADD CONSTRAINT "timeslot_fk0" FOREIGN KEY ("user_id") REFERENCES "users"("user_id");
ALTER TABLE "timeslot" ADD CONSTRAINT "staff_fk1" FOREIGN KEY ("studio_id") REFERENCES "studios"("studio_id");
ALTER TABLE "timeslot" ADD CONSTRAINT "staff_fk2" FOREIGN KEY ("staff_id") REFERENCES "staff"("staff_id");


ALTER TABLE "stuff_timeslot" ADD CONSTRAINT "stuff_timeslot_fk0" FOREIGN KEY ("timeslot_id") REFERENCES "timeslot"("timeslot_id");
ALTER TABLE "stuff_timeslot" ADD CONSTRAINT "stuff_timeslot_fk1" FOREIGN KEY ("staff_id") REFERENCES "staff"("staff_id");









