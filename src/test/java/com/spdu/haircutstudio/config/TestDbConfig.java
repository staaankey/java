package com.spdu.haircutstudio.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import javax.sql.DataSource;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.testcontainers.containers.PostgreSQLContainer;


@TestConfiguration
public class TestDbConfig {

    @Bean(destroyMethod = "close")
    @Primary
    public PostgreSQLContainer<?> postgresSQLContainer() {
        PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>("postgres")
                .withDatabaseName("postgres")
                .withUsername("postgres")
                .withPassword("postgres");

        postgres.start();

        return postgres;
    }

    @Bean(destroyMethod = "close")
    @Primary
    public DataSource getDataSource(PostgreSQLContainer<?> postgresSQLContainer) {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(postgresSQLContainer.getJdbcUrl());
        config.setUsername(postgresSQLContainer.getUsername());
        config.setPassword(postgresSQLContainer.getPassword());

        return new HikariDataSource(config);
    }

}

