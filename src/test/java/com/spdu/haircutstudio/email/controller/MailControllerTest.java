package com.spdu.haircutstudio.email.controller;

import com.spdu.haircutstudio.config.TestDbConfig;
import com.spdu.haircutstudio.email.service.NotificationService;
import com.spdu.haircutstudio.registration.service.UserServiceImpl;
import com.spdu.haircutstudio.registration.web.controller.UserController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootConfiguration
@ContextConfiguration(classes = {TestDbConfig.class})
@ExtendWith(MockitoExtension.class)
class MailControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private MailController controller;

    @Mock
    private NotificationService notificationService;

    @BeforeEach
    void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .build();
    }

    @Test
    void contextLoads() {
    }
}