package com.spdu.haircutstudio.aws.web.controller;

import com.spdu.haircutstudio.aws.service.AwsService;
import com.spdu.haircutstudio.config.TestDbConfig;
import com.spdu.haircutstudio.minio.controller.MinioController;
import com.spdu.haircutstudio.minio.service.MinioService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootConfiguration
@ContextConfiguration(classes = {TestDbConfig.class})
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
//@AutoConfigureMockMvc(secure = false)
@ExtendWith(MockitoExtension.class)
class AwsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private AwsController awsController;

    @Mock
    private AwsService awsService;

    @BeforeEach
    void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(awsController)
                .build();
    }

    @Test
    @Order(2)
    void uploadFile() throws Exception {
        MockMultipartFile firstFile = new MockMultipartFile("file", "filename.jpeg", MediaType.IMAGE_JPEG_VALUE,"content".getBytes());
        mockMvc.perform(MockMvcRequestBuilders.multipart("/aws/buckets/upload")
                    .file(firstFile))
                .andExpect(status().isOk());
    }

    @Test
    @Order(3)
    void downloadFile() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/aws/buckets/download/filename.jpeg"))
                .andExpect(status().isOk());
    }

    @Test
    @Order(4)
    void deleteFile() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/aws/buckets/delete/filename.jpeg"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    @Order(1)
    void createBucket() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/aws/buckets/bucket/testBucket"))
                .andExpect(status().isOk());
    }
}