package com.spdu.haircutstudio.registration.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.spdu.haircutstudio.config.TestDbConfig;
import com.spdu.haircutstudio.registration.service.StaffServiceImpl;
import com.spdu.haircutstudio.registration.util.StaffConverter;
import com.spdu.haircutstudio.registration.web.controller.StaffController;
import com.spdu.haircutstudio.registration.web.dto.StaffDto;
import com.spdu.haircutstudio.registration.web.dto.StaffDtoBuilder;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootConfiguration
@ContextConfiguration(classes = {TestDbConfig.class})
@ExtendWith(MockitoExtension.class)
class StaffControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private StaffController controller;

    @Mock
    private StaffServiceImpl staffService;

    @BeforeEach
    void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .build();
    }

    private static final StaffDto dto = StaffDtoBuilder.aStaffDto()
            .address("testAddress")
            .city("testCity")
            .fullName("testFullName")
            .phone("phone")
            .studioId(1)
            .userId(1)
            .profilePicture("testPhoto")
            .roleId(1)
            .build();

    @Test
    @DisplayName("Register new staff")
    void registerNewStaff() throws Exception {
        mockMvc.perform(post("/staff/new")
                .content(new ObjectMapper().writeValueAsString(dto))
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated());
    }
}