package com.spdu.haircutstudio.registration.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.spdu.haircutstudio.config.TestDbConfig;
import com.spdu.haircutstudio.registration.service.StudioRegistrationServiceImpl;
import com.spdu.haircutstudio.registration.web.controller.StudioController;
import com.spdu.haircutstudio.registration.web.dto.StudioDto;
import com.spdu.haircutstudio.registration.web.dto.StudioDtoBuilder;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootConfiguration
@ContextConfiguration(classes = {TestDbConfig.class})
@ExtendWith(MockitoExtension.class)
class StudioControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private StudioController controller;

    @Mock
    private StudioRegistrationServiceImpl service;

    @BeforeEach
    void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .build();
    }

    private static final StudioDto dto = StudioDtoBuilder.aStudioDto()
            .withStartTime(LocalDateTime.parse("2022-04-25T08:00:00.000"))
            .withEndTime(LocalDateTime.parse("2022-04-25T18:00:00.000"))
            .withName("uniqueRandomName")
            .withUrl("barbershop.com")
            .withPhone("+4049343457")
            .build();


    @Test
    @Order(1)
    @DisplayName("Get all registered barbershops")
    void getALlBarbershops() throws Exception {
        mockMvc.perform(get("/studios/all")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @Order(2)
    @DisplayName("Get barbershop by id")
    void getBarbershopById() throws Exception {
        mockMvc.perform(get("/studios/1")
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

    @Test
    @Order(3)
    @DisplayName("Register new barbershop")
    void registerBarbershop() throws Exception {
        mockMvc.perform(post("/studios/new")
                        .content(new ObjectMapper()
                                .registerModule(new JavaTimeModule())
                                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                                .writeValueAsString(dto)
                        )
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }
}