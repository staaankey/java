package com.spdu.haircutstudio.registration.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spdu.haircutstudio.config.TestDbConfig;
import com.spdu.haircutstudio.registration.service.UserService;
import com.spdu.haircutstudio.registration.service.UserServiceImpl;
import com.spdu.haircutstudio.registration.web.controller.UserController;
import com.spdu.haircutstudio.registration.web.dto.UserDto;
import com.spdu.haircutstudio.registration.web.dto.UserDtoBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootConfiguration
@ContextConfiguration(classes = {TestDbConfig.class})
@ExtendWith(MockitoExtension.class)
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private UserController controller;

    @Mock
    private UserServiceImpl userService;

    @BeforeEach
    void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .build();
    }

    private UserDto dto = UserDtoBuilder.anUserDto()
            .withAddress("randomAddress")
            .withPhone("+3493483843")
            .withCity("randomCity")
            .withFullName("randomName")
            .withPassword("admin")
            .build();

    @Test
    void register() throws Exception {
        mockMvc.perform(post("/users/new")
                        .content(new ObjectMapper()
                                .writeValueAsString(dto)
                        )
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    void getByUserId() throws Exception {
        mockMvc.perform(get("/users/5")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void getAllUsers() throws Exception {
        mockMvc.perform(get("/users/all")
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }
}