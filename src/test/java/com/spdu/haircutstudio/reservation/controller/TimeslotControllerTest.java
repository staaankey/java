package com.spdu.haircutstudio.reservation.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.spdu.haircutstudio.config.TestDbConfig;
import com.spdu.haircutstudio.reservation.service.TimeslotService;
import com.spdu.haircutstudio.reservation.web.controller.TimeslotController;
import com.spdu.haircutstudio.reservation.web.dto.TimeslotDto;
import org.hamcrest.core.Is;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootConfiguration
@ContextConfiguration(classes = {TestDbConfig.class})
@ExtendWith(MockitoExtension.class)
class TimeslotControllerTest {
    @Mock
    private TimeslotService timeslotService;

    @InjectMocks
    private TimeslotController timeslotController;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(timeslotController)
                .build();
    }

    @Test
    void validRegistration() throws Exception {
        TimeslotDto dto = new TimeslotDto();
        dto.setDay(LocalDateTime.parse("2022-04-20T00:00:00.000"));
        dto.setStartTime(LocalDateTime.parse("2022-04-20T12:00:00.000"));
        dto.setEndTime(LocalDateTime.parse("2022-04-20T13:00:00.000"));
        dto.setUserId(1);
        dto.setStudioId(1);
        mockMvc.perform(MockMvcRequestBuilders.post("/timeslots/new")
                        .content(new ObjectMapper()
                                .registerModule(new JavaTimeModule())
                                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                                .writeValueAsString(dto)
                        )
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    void getAllTimeslots() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/timeslots/1"))
                .andExpect(status().isOk());
    }
}