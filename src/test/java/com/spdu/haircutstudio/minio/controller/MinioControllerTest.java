package com.spdu.haircutstudio.minio.controller;

import com.spdu.haircutstudio.config.TestDbConfig;
import com.spdu.haircutstudio.minio.service.MinioService;
import org.junit.jupiter.api.*;
import org.springframework.http.MediaType;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootConfiguration
@ContextConfiguration(classes = {TestDbConfig.class})
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@ExtendWith(MockitoExtension.class)
class MinioControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private MinioController minioController;

    @Mock
    private MinioService minioService;

    @BeforeEach
    void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(minioController)
                .build();
    }

    @Test
    @Order(2)
    void uploadFile() throws Exception {
        MockMultipartFile firstFile = new MockMultipartFile("file", "filename.jpeg", MediaType.IMAGE_JPEG_VALUE,"content".getBytes());
        mockMvc.perform(MockMvcRequestBuilders.multipart("/minio/buckets/upload/testBucket")
                        .file(firstFile))
                .andExpect(status().isCreated());

    }

    @Test
    @Order(1)
    void createBucket() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/minio/buckets/testBucket"))
                .andExpect(status().isCreated());
    }

    @Test
    @Order(3)
    void getImageLink() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/minio/buckets/link/testBucket/filename.jpeg"))
                .andExpect(status().isOk());
    }
}