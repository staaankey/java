[![pipeline status](https://gitlab.com/staaankey/java/badges/main/pipeline.svg)](https://gitlab.com/staaankey/java/-/commits/main)
[![coverage report](https://gitlab.com/staaankey/java/badges/main/coverage.svg)](https://gitlab.com/staaankey/java/-/commits/main)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

## SPD-University course


![Barbershop](image.jpg)
## About Project

**Haircut Studio** designed and developed in educational reasons. This is a pet project. In which I learned to work with the Spring Boot, Spring Security, Flyway frameworks. Docker Containers, JDBC, Swagger, Open API. 

# Core features of the project:
* Register barbershop with custom work hours
* Register barber in barbershop 
* Create an appointment for a specific time

# Development setup
##### 1. ```git clone https://gitlab.com/staaankey/java.git```
##### 2. ```cd java```
##### 3. ```gradle build```
##### 4. ```cd .local```
##### 5. ```docker-compose up```

# Requirements
##### 1. Pre-installed java v17. 
##### 2. Docker on your machine
##### 3. Postgres client
